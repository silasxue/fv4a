
from itertools import product
import numpy as np
from numpy.testing import assert_allclose
import pdb

from utils import import_dataset


def test_fisher_vector():
    """Checks that features computed with `fisher_vector.py` are the same the
    ones already pre-computed. Get the data to compare against following the
    instructions:

    # Create folders.
    mkdir -p data/hollywood2/{videos,filelists,features}
    mkdir -p data/hollywood2/features/dense5.track15mbh/{gmm,pca,statistics_k_50}
    mkdir -p data/hollywood2/features/dense5.track15mbh/statistics_k_50/{fv,sfv}/spm_{111,112,131}

    # Get data:
    # - PCA model
    wget http://pascal.inrialpes.fr/data2/oneata/code/fv4a/data/pca_64 -P data/hollywood2/features/dense5.track15mbh/pca
    # - GMM model
    wget http://pascal.inrialpes.fr/data2/oneata/code/fv4a/data/gmm_50 -P data/hollywood2/features/dense5.track15mbh/gmm
    # - filelists
    wget http://pascal.inrialpes.fr/data2/oneata/code/fv4a/data/{train,test}.list -P data/hollywood2/filelists/
    # - a video
    wget http://pascal.inrialpes.fr/data2/oneata/code/fv4a/data/actioncliptrain00808.avi -P data/hollywood2/videos/

    # Extract FV and SFV for the given video.
    python fisher_vector.py -d hollywood2.small -b 0 -e 1 -np 1 -vv

    # Get the expected descriptors.
    mkdir -p data/hollywood2/features/dense5.track15mbh/expected_statistics_k_50/{fv,sfv}/spm_{111,112,131}
    for m in fv sfv; do \
        for g in 111 112 131; do \
            wget http://pascal.inrialpes.fr/data2/oneata/code/fv4a/data/${m}/spm_${g}/actioncliptrain00808.dat -P data/hollywood2/features/dense5.track15mbh/expected_statistics_k_50/${m}/spm_${g}; \
        done; \
    done

    # Check that the expected descriptors match the given
    nosetests -s tests/check.py
    """
    SAMPLE = "actioncliptrain00808"
    MODELS = ['FV', 'SFV']
    SPMS = [(1, 1, 1), (1, 1, 2), (1, 3, 1)]
    dataset = import_dataset('hollywood2', 'small')
    for model, spm in product(MODELS, SPMS):
        path_actual = dataset.get_feature_path(SAMPLE, model, spm)
        path_expected = path_actual.replace('statistics', 'expected_statistics')
        X = np.atleast_2d(np.fromfile(path_actual, dtype=np.float32))
        Y = np.atleast_2d(np.fromfile(path_expected, dtype=np.float32))
        assert_allclose(X, Y)

